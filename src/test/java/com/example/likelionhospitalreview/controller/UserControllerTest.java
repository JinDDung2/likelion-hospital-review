package com.example.likelionhospitalreview.controller;

import com.example.likelionhospitalreview.config.SecurityConfig;
import com.example.likelionhospitalreview.domain.dto.user.JoinRequest;
import com.example.likelionhospitalreview.exception.DuplicateUsernameException;
import com.example.likelionhospitalreview.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
@ImportAutoConfiguration(SecurityConfig.class)
class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserService userService;
    private ObjectMapper objectMapper = new ObjectMapper();
    @Test
    void join() throws Exception{
        String USERNAME = "tester";
        String PASSWORD = "qwer1234";
        JoinRequest joinRequest = new JoinRequest(USERNAME, PASSWORD);

        given(userService.join(any(JoinRequest.class))).willReturn("회원가입 완료");

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/users/join")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(joinRequest))
        ).andExpect(status().isOk())
        .andDo(print());
    }

    @Test
    void join_DuplicationUsernameException() throws Exception{
        String USERNAME = "tester";
        String PASSWORD = "qwer1234";
        JoinRequest joinRequest = new JoinRequest(USERNAME, PASSWORD);
        DuplicateUsernameException e = new DuplicateUsernameException();

        given(userService.join(any(JoinRequest.class))).willThrow(e);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/users/join")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(joinRequest))
                ).andExpect(status().isConflict())
                .andExpect(jsonPath("$.code").value(409))
                .andExpect(jsonPath("$.message").value(e.getMessage()))
                .andDo(print());
    }
}