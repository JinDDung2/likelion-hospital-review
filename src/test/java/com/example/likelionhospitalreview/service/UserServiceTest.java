package com.example.likelionhospitalreview.service;

import com.example.likelionhospitalreview.domain.dto.user.JoinRequest;
import com.example.likelionhospitalreview.domain.entity.User;
import com.example.likelionhospitalreview.exception.DuplicateUsernameException;
import com.example.likelionhospitalreview.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

class UserServiceTest {
    private final UserRepository userRepository = Mockito.mock(UserRepository.class);
    private final PasswordEncoder passwordEncoder = Mockito.mock(PasswordEncoder.class);
    private final UserService userService = new UserService(userRepository, passwordEncoder);

    private final String USERNAME = "tester";
    private final String PASSWORD = "qwer123";
    private final JoinRequest joinRequest = new JoinRequest(USERNAME, PASSWORD);

    @Test
    void join_아이디_중복시_DuplicateUsernameException() {
        given(userRepository.existsByUsername("tester")).willReturn(true); // username 중복

        assertThrows(DuplicateUsernameException.class, () -> userService.join(joinRequest));
    }

    @Test
    void join() {
        final String EXPECTED = "회원가입 완료";

        given(userRepository.existsByUsername("tester")).willReturn(false);
        given(userRepository.save(any(User.class))).willReturn(joinRequest.toEntity(PASSWORD));

        assertEquals(EXPECTED, userService.join(joinRequest));
    }
}