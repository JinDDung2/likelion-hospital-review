package com.example.likelionhospitalreview.service;

import com.example.likelionhospitalreview.domain.dto.user.JoinRequest;
import com.example.likelionhospitalreview.exception.DuplicateUsernameException;
import com.example.likelionhospitalreview.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public String join(JoinRequest joinRequest) {
        if (userRepository.existsByUsername(joinRequest.getUsername())) throw new DuplicateUsernameException();
        userRepository.save(joinRequest.toEntity(passwordEncoder.encode(joinRequest.getPassword())));
        return "회원가입 완료";
    }
}
