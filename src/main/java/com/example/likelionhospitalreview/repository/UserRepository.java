package com.example.likelionhospitalreview.repository;

import com.example.likelionhospitalreview.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByUsername(String username);
}
