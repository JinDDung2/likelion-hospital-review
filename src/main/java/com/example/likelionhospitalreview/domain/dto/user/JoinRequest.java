package com.example.likelionhospitalreview.domain.dto.user;

import com.example.likelionhospitalreview.domain.entity.User;
import lombok.Getter;

@Getter
public class JoinRequest {
    private String username;
    private String password;

    private JoinRequest() {
    }

    public JoinRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User toEntity(String encoded) {
        return User.builder()
                .username(username)
                .password(encoded)
                .build();
    }
}
